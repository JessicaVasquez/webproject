<?php include("../template/header.php"); ?>
<?php include("../funciones/conexion.php"); ?>
<?php

$valorRut = $_POST['ingresaRut'];

/*
$sentenciaSQL = $conexion->prepare("SELECT * FROM desconocimiento INNER JOIN producto ON desconocimiento.productoid = producto.id 
    INNER JOIN cliente ON desconocimiento.productocliente_id = cliente.id INNER JOIN sucursal ON desconocimiento.productocliente_sucursal_id = sucursal.id 
    WHERE cliente.rut = '$valorRut'");
*/

$sentenciaSQL = $conexion->prepare("SELECT * FROM producto INNER JOIN cliente ON producto.cliente_id = cliente.id INNER JOIN sucursal 
ON producto.cliente_sucursal_id = sucursal.id WHERE cliente.rut = '$valorRut'");

$sentenciaSQL->execute();
$listaClientes = $sentenciaSQL->fetchAll(PDO::FETCH_ASSOC);

?>

<?php

$salesforce = (isset($_POST['salesforce'])) ? $_POST['salesforce'] : "";
$siniestro = (isset($_POST['siniestro'])) ? $_POST['siniestro'] : "";
$apertura = (isset($_POST['apertura'])) ? $_POST['apertura'] : "";
$bloqueo = (isset($_POST['bloqueo'])) ? $_POST['bloqueo'] : "";
$recepcion = (isset($_POST['recepcion'])) ? $_POST['recepcion'] : "";
$monto_pesos = (isset($_POST['monto_pesos'])) ? $_POST['monto_pesos'] : "";
$monto_dolares = (isset($_POST['monto_dolares'])) ? $_POST['monto_dolares'] : "";
$causal = (isset($_POST['causal'])) ? $_POST['causal'] : "";
$transaccion = (isset($_POST['transaccion'])) ? $_POST['transaccion'] : "";
$num_transacciones = (isset($_POST['num_transacciones'])) ? $_POST['num_transacciones'] : "";
$detalle_transacciones = (isset($_POST['detalle_transacciones'])) ? $_POST['detalle_transacciones'] : "";

$accion = (isset($_POST['accion'])) ? $_POST['accion'] : "";

switch ($accion) {
    case "Agregar":

        $sentenciaSQL = $conexion->prepare("INSERT INTO desconocimiento (salesforce, siniestro, apertura, bloqueo, 
            recepcion, monto_pesos, monto_dolares, causal, tipo_transaccion, num_transaccion, detalle_transaccion, 
            productoid, productocliente_id, productocliente_sucursal_id) 
            VALUES (:salesforce, :siniestro, :apertura, :bloqueo, :recepcion, :sucursal, :segmentacion, 
            :monto_pesos, :monto_dolares, :causal, :tipo_transaccion, :num_transaccion, :detalle_transaccion, :productoID, :clienteID, :sucursalID);
            ");


        $sentenciaSQL->bindParam(':salesforce', $salesforce);
        $sentenciaSQL->bindParam(':siniestro', $siniestro);
        $sentenciaSQL->bindParam(':apertura', $apertura);
        $sentenciaSQL->bindParam(':bloqueo', $bloqueo);
        $sentenciaSQL->bindParam(':recepcion', $recepcion);
        $sentenciaSQL->bindParam(':monto_pesos', $monto_pesos);
        $sentenciaSQL->bindParam(':monto_dolares', $monto_dolares);
        $sentenciaSQL->bindParam(':causal', $causal);
        $sentenciaSQL->bindParam(':tipo_transaccion', $tipo_transaccion);
        $sentenciaSQL->bindParam(':num_transaccion', $num_transaccion);
        $sentenciaSQL->bindParam(':detalle_transaccion', $detalle_transaccion);
        $sentenciaSQL->bindParam(':productoid', $productoID);
        $sentenciaSQL->bindParam(':productocliente_id', $clienteID);
        $sentenciaSQL->bindParam(':productocliente_sucursal_id', $sucursalID);
        $sentenciaSQL->execute();

        break;
}

?>


<div class="col-md-12"></br></br>
    <div class="card">
        <div class="card-header">
            Nuevo Registro
        </div>
        <div class="card-body">
            <form method="POST">
                <div class="row">
                    <?php
                    foreach ($listaClientes as $desconocimiento) {
                    ?>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="rut">Rut de Cliente</label>
                                <input type="text" class="form-control" id="rut" name="rut" value="<?php echo $desconocimiento['rut'] ?>" disabled />
                                <input type="hidden" class="form-control" id="clienteID" name="clienteID" value="<?php echo $desconocimiento['cliente_id'] ?>" disabled />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="nomCliente">Nombres Cliente</label>
                                <input type="text" class="form-control" id="nombres" name="nombres" value="<?php echo $desconocimiento['nombres'] ?>" disabled />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="a_paterno">Apellido Paterno Cliente</label>
                                <input type="text" class="form-control" id="a_paterno" name="a_paterno" value="<?php echo $desconocimiento['a_paterno'] ?>" disabled />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="a_materno">Apellido Materno Cliente</label>
                                <input type="text" class="form-control" id="a_materno" name="a_materno" value="<?php echo $desconocimiento['a_materno'] ?>" disabled />
                            </div>
                        </div>
                </div><br>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nombre_sucursal">Sucursal</label>
                            <input type="text" class="form-control" name="nombre_sucursal" id="nombre_sucursal" value="<?php echo $desconocimiento['nombre_sucursal'] ?>" disabled />
                            <input type="hidden" class="form-control" id="sucursalID" name="sucursalID" value="<?php echo $desconocimiento['sucursal_id'] ?>" disabled />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="segmento">Segmento</label>
                            <input type="text" class="form-control" name="segmento" id="segmento" value="<?php echo $desconocimiento['segmento'] ?>" disabled />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="item">Item</label>
                                <input type="text" class="form-control" name="item" id="item" value="<?php echo $desconocimiento['item'] ?>" disabled />
                                <input type="hidden" class="form-control" name="productoID" id="productoID" value="<?php echo $desconocimiento['id'] ?>" disabled />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="tipo_tarjeta">Tipo Tarjeta</label>
                                <input type="text" class="form-control" name="tipo_tarjeta" id="tipo_tarjeta" value="<?php echo $desconocimiento['tipo_tarjeta'] ?>" disabled />
                            </div>
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="num_tarjeta">N° de Tarjeta</label>
                            <input type="text" class="form-control" name="num_tarjeta" id="num_tarjeta" value="<?php echo $desconocimiento['num_tarjeta'] ?>" disabled />
                        </div>
                    </div>
                <?php
                    }
                ?>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="nsalesforce">N°Salesforce</label>
                        <input type="text" class="form-control" name="salesforce" id="salesforce" placeholder="N°Salesforce">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="fechaSiniestro">Fecha Siniestro</label>
                        <input type="date" class="form-control" name="siniestro" id="siniestro" placeholder="Fecha de Siniestro">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="fechaApertura">Fecha Apertura</label>
                        <input type="date" class="form-control" name="apertura" id="apertura" placeholder="Fecha de Apertura">
                    </div>
                </div>
                </div><br>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="fechaBloqueo">Fecha Bloqueo</label>
                            <input type="date" class="form-control" name="bloqueo" id="bloqueo" placeholder="Fecha de Bloqueo">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="fechaRecepcion">Fecha Recepción</label>
                            <input type="date" class="form-control" name="recepcion" id="recepcion" placeholder="Fecha de Recepción">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="montoPesos">Monto Reclamado $</label>
                            <input type="text" class="form-control" name="monto_pesos" id="monto_pesos" placeholder="Monto Reclamado $">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="montoUSD">Monto Reclamado USD$</label>
                            <input type="text" class="form-control" name="monto_dolares" id="monto_dolares" placeholder="Monto Reclamado USD$">
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-md-3"><br>
                        <div class="form-group">
                            <label for="causal">Causal:</label>
                            <select name="causal" id="causal">
                                <option selected="selected">Seleccione</option>
                                <option>Fraude</option>
                                <option>Extravio</option>
                                <option>Hurto</option>
                                <option>Robo</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3"><br>
                        <div class="form-group">
                            <label for="tipo_transaccion">Tipo:</label>
                            <select name="tipo_transaccion" id="tipo_transaccion">
                                <option selected="selected">Seleccione</option>
                                <option>Compra en Comercio</option>
                                <option>Compra y Giro ATM</option>
                                <option>Giro ATM</option>
                                <option>Transferencia</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="num_transaccion">N° Transacciones</label>
                            <input type="text" class="form-control" name="num_transaccion" id="num_transaccion" placeholder="N° Transacciones">
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="detalle_transacciones">Detalle de Transacciones</label>
                            <input type="text" class="form-control" name="detalle_transacciones" id="detalle_transacciones" placeholder="Detalle Transacciones">
                        </div>
                    </div>
                </div><br>
                <input type="submit" name="accion" value="Agregar" class="btn btn-success" />
            </form>
        </div>
    </div>
</div>


<?php include("../template/footer.php"); ?>