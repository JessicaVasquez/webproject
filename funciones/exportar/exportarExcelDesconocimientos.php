<?php

//Exportar Desconocimientos

$host="localhost";
$bd="mydb";
$usuario="root";
$contrasena="";

$export = "";

$connect = mysqli_connect("$host", "$usuario", "$contrasena", "$bd");

$fecha=date('d_m_Y');

//Exportar datos a Excel
    if(isset($_POST["submit"])){
    $query = "SELECT * FROM desconocimiento INNER JOIN producto ON desconocimiento.productoid = producto.id 
    INNER JOIN cliente ON desconocimiento.productocliente_id = cliente.id INNER JOIN sucursal ON desconocimiento.productocliente_sucursal_id = sucursal.id";
    $res = mysqli_query($connect, $query);
        if(mysqli_num_rows($res) > 0){
        $export .= '
        <table> 
            <tr> 
                <th>N Salesforce</th>
                <th>RUT Cliente</th>
                <th>Nombre Cliente</th>
                <th>Apellido Materno</th>
                <th>Apellido Materno</th>
                <th>Sucursal</th>
                <th>Segmentacion</th>
                <th>Monto Reclamado $</th>
                <th>Monto Reclamado US$</th>
                <th>Item</th>
                <th>Tipo Tarjeta</th>
                <th>N Tarjeta</th>
                <th>Fecha Siniestro</th>
                <th>Fecha Apertura</th>
                <th>Fecha Bloqueo</th>
                <th>Fecha Abono UF35</th>
                <th>Tipo Causal Ley</th>
                <th>Tipo de Transaccion</th>
                <th>Transacciones Reclamadas</th>
                <th>Detalle de Transaccion</th> 
            </tr>
        ';
        while($row = mysqli_fetch_array($res))
        {
        $export .= '
        <tr>
            <td>'.$row["salesforce"].'</td>
            <td>'.$row["rut"].'</td>
            <td>'.$row["nombres"].'</td>
            <td>'.$row["a_paterno"].'</td>
            <td>'.$row["a_materno"].'</td>
            <td>'.$row["nombre_sucursal"].'</td>
            <td>'.$row["segmento"].'</td>
            <td>'.$row["monto_pesos"].'</td>
            <td>'.$row["monto_dolares"].'</td>
            <td>'.$row["item"].'</td>
            <td>'.$row["tipo_tarjeta"].'</td>
            <td>'.$row["num_tarjeta"].'</td>
            <td>'.$row["siniestro"].'</td>
            <td>'.$row["apertura"].'</td>
            <td>'.$row["bloqueo"].'o</td>
            <td>'.$row["recepcion"].'</td>
            <td>'.$row["tipo_transaccion"].'</td>
            <td>'.$row["causal"].'</td>
            <td>'.$row["num_transaccion"].'</td>
            <td>'.$row["detalle_transaccion"].'</td>             
        </tr>
        ';
        }
        $export .= '</table>';
        header('Pragma: no-cache');
        header('Expires: 0');
        header('Content-Transfer-Encoding: none');
        header('Content-type: application/vnd.ms-excel;charset=utf-8');
        header("Content-Disposition: attachment; filename=Reporte_$fecha.xls");
        echo $export;
        }
    }
?>