<?php

include_once 'funciones/checkUser.php';
//include_once 'includes/user_session.php';

$userSession = new UserSession();
$user = new User();

if ( isset($_SESSION['usuario']) ) {
  //echo "Hay sesión";
  $user->setUser($userSession->getCurrentUser());
  include_once 'view_page/loginDesconocimiento.php';
} else if ( isset($_POST['ingresaRut']) ){
    //echo "Validación de login";

    $userForm = $_POST['ingresaRut'];

    if($user->userExists($userForm)){
      //echo "usuario validado";
      //$userSession->setCurrentUser($userForm);
      //$user->setUser($userForm);

      include_once 'view_page/ingresarDesconocimiento.php';
    }else{
        //echo "nombre de usuario y/o password incorrecto";
        $errorLogin = "Rut no encontrado";
        include_once 'view_page/loginDesconocimiento.php';
    }

} else {
  //echo "Login";
  include_once 'view_page/loginDesconocimiento.php';
}


?>