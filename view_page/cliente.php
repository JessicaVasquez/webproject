<?php include("../template/header.php"); ?>
<?php include("../funciones/conexion.php"); ?>
<?php
$sentenciaSQL = $conexion->prepare("SELECT * FROM cliente INNER JOIN sucursal ON cliente.sucursal_id = sucursal.id");
$sentenciaSQL->execute();
$listaUsuarios = $sentenciaSQL->fetchAll(PDO::FETCH_ASSOC);
?>

<section class="page-section clearfix">
    <div class="row">
        <div class="col-md">
            <div class="btn-group" role="group" aria-label="">
                <form method="POST" action="../funciones/exportar/exportarExcelUsuarios.php">
                    <input type="submit" name="submit" value="Exportar Registros" class="btn btn-danger">
                </form>
                <input type="submit" name="submit" value="Importar Registros" class="btn btn-success" data-toggle="modal" data-target="#modalImportar">
            </div>
        </div>
    </div><br>
    <div class="row">
        <div class="col-md-12">
            <input class="form-control" id="myInput" type="text" placeholder="Buscar...">
        </div>
    </div><br>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Nuestros Clientes
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Acciones</th>
                                <th scope="col">RUT</th>
                                <th scope="col">Nombres de Usuario</th>
                                <th scope="col">A. Paterno</th>
                                <th scope="col">A. Materno</th>
                                <th scope="col">Sucursal</th>
                                <th scope="col">Segmento</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                            <?php
                            foreach ($listaUsuarios as $usuario) {
                            ?>
                                <tr class="table-light">
                                    <td>
                                        <div class="btn-group mr-2 btn-group-sm" role="group">
                                            <button type="btn" class="btn btn-xs"><i class="fa fa-eye"></i></button>
                                            <button class="btn" class="btn btn-xs"><i class="fa fa-plus"></i></button>
                                            <a class="btn btn-xs" href="../funciones/delete.php?id=<?php echo $usuario['rut'];?>"><i class="fa-solid fa-trash-can"></i></a>
                                        </div>
                                    </td>
                                    <td><?php echo $usuario['rut'] ?></td>
                                    <td><?php echo $usuario['nombres'] ?></td>
                                    <td><?php echo $usuario['a_paterno'] ?></td>
                                    <td><?php echo $usuario['a_materno'] ?></td>
                                    <td><?php echo $usuario['nombre_sucursal'] ?></td>
                                    <td><?php echo $usuario['segmento'] ?></td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
</section>

<!-- Modal Importar Datos -->
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modalImportar">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Importar Registro</h5>
                <button type="button" class="btn-close" data-dismiss="modal" aria-label="Cerrar"></button>
            </div>
            <div class="modal-body">
                <form action="../funciones/importar/importar_excel_usuario.php" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-6">
                            <div class="file-input text-center">
                                <label for="formFileMultiple" class="form-label">Seleccionar Archivo CSV</label>
                                <input type="file" name="dataCliente" id="file-input" class="file-input__input">
                            </div>
                        </div><br>
                        <div class="col-6">
                            <input type="submit" name="subir" class="btn btn-primary" value="Subir Archivo" />
                        </div>
                    </div>                           
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>


<?php include("../template/footer.php"); ?>