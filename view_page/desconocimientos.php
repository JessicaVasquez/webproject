<?php include("../template/header.php"); ?>
<?php include("../funciones/conexion.php"); ?>
<?php

$sentenciaSQL = $conexion->prepare("SELECT * FROM desconocimiento INNER JOIN producto ON desconocimiento.productoid = producto.id 
     INNER JOIN cliente ON desconocimiento.productocliente_id = cliente.id INNER JOIN sucursal ON desconocimiento.productocliente_sucursal_id = sucursal.id");

$sentenciaSQL->execute();
$listaDesconocimientos = $sentenciaSQL->fetchAll(PDO::FETCH_ASSOC);
?>
<section class="page-section"><br>
    <div class="row">
        <div class="col-md-12">
            <div class="btn-group" role="group" aria-label="">
                <form method="POST" action="../funciones/exportar/exportarExcelDesconocimientos.php"">
                    <input type=" submit" name="submit" value="Exportar Registros" class="btn btn-danger">
                </form>
                <button type="button" value="Importar" class="btn btn-success">Importar Registros</button>
            </div>
        </div>
    </div><br>
    <div class="row">
        <div class="col-md-12">
            <input class="form-control" id="myInput" type="text" placeholder="Buscar...">
        </div>
    </div><br>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Desconocimientos
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Acciones</th>
                                <th scope="col">N° Salesforce</th>
                                <th scope="col">RUT Cliente</th>
                                <th scope="col">Nombres Cliente</th>
                                <th scope="col">Apellido Paterno</th>
                                <th scope="col">Apellido Materno</th>
                                <th scope="col">Monto Reclamado $</th>
                                <th scope="col">Monto Reclamado US$</th>
                                <th scope="col">Tipo de Caso</th>
                                <th scope="col">Tipo Tarjeta</th>
                                <th scope="col">N° Tarjeta</th>
                                <th scope="col">Fecha Siniestro</th>
                                <th scope="col">Fecha Apertura</th>
                                <th scope="col">Fecha Bloqueo</th>
                                <th scope="col">Fecha Recepción</th>
                                <th scope="col">Sucursal</th>
                                <th scope="col">Segmentación</th>
                                <th scope="col">Tipo Causal Ley</th>
                                <th scope="col">Tipo de Transacción</th>
                                <th scope="col">Transacciones Reclamadas</th>
                                <th scope="col">Detalle de Transacción</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                            <?php
                            foreach ($listaDesconocimientos as $desconocimiento) {
                            ?>
                                <tr class="table-light">
                                    <td>
                                        <div class="btn-group mr-2 btn-group-sm" role="group">
                                            <button class="btn btn-xs"><i class="fa fa-eye"></i></button>
                                            <button class="btn btn-xs"><i class="fa fa-plus"></i></button>
                                            <button class="btn btn-xs"><i class="fa-solid fa-trash-can"></i></button>
                                        </div>
                                    </td>
                                    <td><?php echo $desconocimiento['salesforce'] ?></td>
                                    <td><?php echo $desconocimiento['rut'] ?></td>
                                    <td><?php echo $desconocimiento['nombres'] ?></td>
                                    <td><?php echo $desconocimiento['a_paterno'] ?></td>
                                    <td><?php echo $desconocimiento['a_materno'] ?></td>
                                    <td><?php echo $desconocimiento['monto_pesos'] ?></td>
                                    <td><?php echo $desconocimiento['monto_dolares'] ?></td>
                                    <td><?php echo $desconocimiento['item'] ?></td>
                                    <td><?php echo $desconocimiento['tipo_tarjeta'] ?></td>
                                    <td><?php echo $desconocimiento['num_tarjeta'] ?></td>
                                    <td><?php echo $desconocimiento['siniestro'] ?></td>
                                    <td><?php echo $desconocimiento['apertura'] ?></td>
                                    <td><?php echo $desconocimiento['bloqueo'] ?></td>
                                    <td><?php echo $desconocimiento['recepcion'] ?></td>
                                    <td><?php echo $desconocimiento['nombre_sucursal'] ?></td>
                                    <td><?php echo $desconocimiento['segmento'] ?></td>
                                    <td><?php echo $desconocimiento['causal'] ?></td>
                                    <td><?php echo $desconocimiento['tipo_transaccion'] ?></td>
                                    <td><?php echo $desconocimiento['num_transaccion'] ?></td>
                                    <td><?php echo $desconocimiento['detalle_transaccion'] ?></td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>


<?php include("../template/footer.php"); ?>