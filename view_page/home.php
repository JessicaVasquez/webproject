<?php include("../template/header.php"); ?>
<div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
    <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
        <div class="carousel-item active" data-bs-interval="10000">
            <img src="../assets/img/BICE/bice_4.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
                <h1 class="text-center text-faded d-none d-md-block">
                    <span class="site-heading-upper text-primary mb-3">Banco BICE</span>
                    <span class="site-heading-upper">Un Mundo de Beneficios con el BICE</span>
                </h1>
            </div>
        </div>
        <div class="carousel-item" data-bs-interval="2000">
            <img src="../assets/img/BICE/bice_2.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
                <h1 class="text-center text-faded d-none d-md-block">
                    <span class="site-heading-upper text-primary mb-3">Seguros Familiares</span>
                    <span class="site-heading-upper">Protege a tu famila 24/7</span>
                </h1>
            </div>
        </div>
        <div class="carousel-item">
            <img src="../assets/img/BICE/bice_3.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
                <h1 class="text-center text-faded d-none d-md-block">
                    <span class="site-heading-upper text-primary mb-3">Consigue tu Casa Propia</span>
                    <span class="site-heading-upper">Sueña en Grande con Banco BICE</span>
                </h1>
            </div>
        </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Anterior</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Posterior</span>
    </button>
</div>


<?php include("../template/footer.php"); ?>