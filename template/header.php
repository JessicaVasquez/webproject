<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Desconocimientos Banco BICE</title>
    <link rel="icon" type="image/x-icon" href="../assets/favicon.ico" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet" />

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- jQuery UI -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

    <!--Gráficos-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.8.0/chart.min.js"></script>

    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="../css/styles.css" rel="stylesheet" />
</head>

<body>
    <header>
        <h1 class="site-heading text-center text-faded d-none d-md-block">
            <span class="site-heading-upper text-primary mb-3">Desconocimiento de Transacciones</span>
            <span class="site-heading-lower">Banco BICE</span>
        </h1>
    </header>

    <?php $url = "http://" . $_SERVER['HTTP_HOST'] . "/webpage/view_page/"; ?>

    <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
        <div class="container">
            <a class="navbar-brand text-uppercase fw-bold d-lg-none" href="/view/index.html">Desconocimientos</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item px-lg-4" id="tab_home"><a class="nav-link text-uppercase" href="<?php echo $url; ?>home.php">Home</a></li>
                    <li class="nav-item px-lg-4" id="tab_cliente"><a class="nav-link text-uppercase" href="<?php echo $url; ?>cliente.php">Clientes</a></li>
                    <li class="nav-item px-lg-4" id="tab_desconocimientos"><a class="nav-link text-uppercase" href="<?php echo $url; ?>desconocimientos.php">Desconocimientos</a></li>
                    <li class="nav-item px-lg-4" id="loginDesconocimiento"><a class="nav-link text-uppercase" href="<?php echo $url; ?>handleConsultaDesconocimiento.php">Ingresar Desconocimiento</a></li>
                    <li class="nav-item px-lg-4" id="tab_graficos"><a class="nav-link text-uppercase" href="<?php echo $url; ?>graficos.php">Estadísticas</a></li>
                    <li class="nav-item px-lg-4" id="tab_graficos"><a class="nav-link text-uppercase" href="<?php echo $url; ?>graficos.php">Mi Perfil</a></li>
                    <li class="nav-item px-lg-4" id="tab_graficos"><a class="nav-link text-uppercase" href="<?php echo $url; ?>graficos.php">Cerrar Sesión</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container p-2">
        <div class="row">
