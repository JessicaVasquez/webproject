<?php include("../template/header.php"); ?>

<div class="row">
    <div class="col-6">
        <div class="card text-left">
            <div class="card-body">
                <h4 class="card-title">Monto de Desconocimiento por Ítem</h4>
                <p class="card-text">Ítems</p>
                <canvas id="chartBar"></canvas>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="card text-left">
            <div class="card-body">
                <h4 class="card-title">Title</h4>
                <p class="card-text">Body</p>
                <canvas id="chartBar"></canvas>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <!-- Otro Gráfico -->
    </div>
    <div class="col-6">
        <!-- Otro Gráfico -->
    </div>
</div>

<?php

$con = new mysqli('localhost', 'root', '', 'mydb');

$query_barra = $con->query("SELECT producto.item as tarjeta, SUM(monto_pesos) as monto FROM desconocimiento INNER JOIN producto 
ON desconocimiento.productoid=productocliente_id group by producto.item;");

foreach ($query_barra as $data) {
    $tarjetaItem[] = $data['tarjeta'];
    $montoTarjeta[] = $data['monto'];
}

?>


<!--Gráfico Bar Char-->
<script>
    const data = {
        labels: <?php echo json_encode($tarjetaItem) ?>,
        datasets: [{
            label: 'Monto Acumulado por Ítem',
            data: <?php echo json_encode($montoTarjeta) ?>,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(255, 205, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(201, 203, 207, 0.2)'
            ],
            borderColor: [
                'rgb(255, 99, 132)',
                'rgb(255, 159, 64)',
                'rgb(255, 205, 86)',
                'rgb(75, 192, 192)',
                'rgb(54, 162, 235)',
                'rgb(153, 102, 255)',
                'rgb(201, 203, 207)'
            ],
            borderWidth: 1
        }]
    };
    const config = {
        type: 'bar',
        data: data,
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        },
    };
    const chartBar = new Chart(
        document.getElementById('chartBar'),
        config
    );
</script>



<?php include("../template/footer.php"); ?>